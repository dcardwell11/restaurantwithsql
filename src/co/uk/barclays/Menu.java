package co.uk.barclays;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Menu {
    private String title;
    private int restaurantId;
    private int id;

    public static ArrayList<Menu> all = new ArrayList<>();

    public static void init(){
        try{
        Statement createMenuTable = DB.conn.createStatement();
        createMenuTable.execute("CREATE TABLE IF NOT EXISTS menus (id INTEGER PRIMARY KEY, restaurantId INTEGER, Title TEXT);");
        Statement getMenus = DB.conn.createStatement();
        ResultSet menus = getMenus.executeQuery("SELECT * FROM menus");
        while(menus.next()){
            int id = menus.getInt(1);
            int restaurantId = menus.getInt(2);
            String title = menus.getString(3);
            new Menu(id, restaurantId, title);
        }
    }catch(SQLException err){
            System.out.println(err.getMessage());
        }
    }
    public Menu(int restaurantId, String title){
        //init();
        this.title = title;
        this.restaurantId = restaurantId;
        try{
            PreparedStatement insertMenu = DB.conn.prepareStatement("INSERT INTO menus (restaurantId, Title) VALUES (?,?);");
            insertMenu.setInt(1, this.restaurantId);
            insertMenu.setString(2, this.title);
            insertMenu.executeUpdate();
            this.id = insertMenu.getGeneratedKeys().getInt(1);
        }catch(SQLException err){
            System.out.println("~~~"+err.getMessage());
        }
        Menu.all.add(this);

    }
    public Menu(int id, int restaurantId, String title){
        this.id=id;
        this.restaurantId=restaurantId;
        this.title=title;
        Menu.all.add(this);
    }
    public int getId() {
        return this.id;
    } 
    public String getTitle() {
        return this.title;
    }
    public int getRestaurantId() {
        return this.restaurantId;
    }
    public ArrayList<Item> getMenuItems(){
        ArrayList<Item> menuItems = new ArrayList<Item>();
        for(int i = 0; i<Item.all.size();i++){
            if(Item.all.get(i).getMenuId() == this.id){
                menuItems.add(Item.all.get(i));
            }
        }
        return menuItems;
    }
    
}
