package co.uk.barclays;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Restaurant {
    private String name;
    private String imageURL;
    private int id;

    public static ArrayList<Restaurant> all= new ArrayList<>();

    public static void init(){
        try{
        Statement createTable = DB.conn.createStatement();
        createTable.execute("CREATE TABLE IF NOT EXISTS restaurants (id INTEGER PRIMARY KEY, name TEXT, imageURl TEXT);");
        Statement getRestaurants = DB.conn.createStatement();
        ResultSet restaurants = getRestaurants.executeQuery("SELECT * FROM restaurants");
            while(restaurants.next()){
                int id = restaurants.getInt(1);
                String name = restaurants.getString(2);
                String imageURL = restaurants.getString(3);
                new Restaurant(id, name, imageURL);
            }
        } catch(SQLException err){}
    }
    public Restaurant(String name, String imageURL){
        this.name = name;
        this.imageURL =imageURL;
        try{
            PreparedStatement insertRestaurant = DB.conn.prepareStatement("INSERT INTO restaurants (name, imageURL) VALUES (?,?);");
            insertRestaurant.setString(1, this.name);
            insertRestaurant.setString(2, this.imageURL);
            insertRestaurant.executeUpdate();
            this.id = insertRestaurant.getGeneratedKeys().getInt(1);
        }catch(SQLException err){}
        Restaurant.all.add(this);
    }
    public Restaurant(int id,String name, String imageURL){
        this.id=id;
        this.name = name;
        this.imageURL =imageURL;
        Restaurant.all.add(this);
    }
    public int getId() {
        return this.id;
    }
 
    public String getName() {
        return this.name;
    }
    public String getImageURL() {
        return this.imageURL;
    }
    public ArrayList<Menu> getRestaurantMenus(){
        ArrayList<Menu> restaurantMenus = new ArrayList<Menu>();
        for(int i = 0; i<Menu.all.size();i++){
            if(Menu.all.get(i).getRestaurantId() == this.id){
                restaurantMenus.add(Menu.all.get(i));
            }
        }
        return restaurantMenus;
    }
    public ArrayList<Item> getItems(){
        ArrayList<Item> arr = new ArrayList<Item>();
        for(int i = 0; i<Menu.all.size();i++){
            if(Menu.all.get(i).getRestaurantId() == this.id){
                int a = Menu.all.get(i).getId();
                for(int j = 0;j<Item.all.size();j++){
                    if(Item.all.get(j).getMenuId() == a){
                        arr.add(Item.all.get(j));
                    }
                }
                
            }
        }
        return arr;
    }
    
}
