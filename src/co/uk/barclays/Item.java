package co.uk.barclays;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Item {
    private String name;
    private int menuId;
    private int id;
    private double price;

    public static ArrayList<Item> all = new ArrayList<>();

    public static void init(){
        try{
        Statement createItemTable = DB.conn.createStatement();
        createItemTable.execute("CREATE TABLE IF NOT EXISTS items (id INTEGER PRIMARY KEY, menuId INTEGER, name TEXT, price DOUBLE);");
        Statement getItems = DB.conn.createStatement();
        ResultSet items = getItems.executeQuery("SELECT * FROM items");
        while(items.next()){
            int id = items.getInt(1);
            int menuId = items.getInt(2);
            String name = items.getString(3);
            Double price = items.getDouble(4);
            new Item(id, menuId, name, price);
        }
        }catch(SQLException err){
            System.out.println(err.getMessage());
        }
    }
    public Item(int menuId, String name, double price){
        //init();
        this.name = name;
        this.menuId = menuId;
        this.price = price;
        try{
            PreparedStatement insertItem = DB.conn.prepareStatement("INSERT INTO items (menuId, name, price) VALUES (?,?,?);");
            insertItem.setInt(1, this.menuId);
            insertItem.setString(2, this.name);
            insertItem.setDouble(3, this.price);
            insertItem.executeUpdate();
            this.id = insertItem.getGeneratedKeys().getInt(1);
        }catch(SQLException err){
            System.out.println("###"+err.getMessage());
        }
        Item.all.add(this);

    }
    public Item(int id, int  menuId, String name, double price){
        this.id=id;
        this.menuId=menuId;
        this.name=name;
        this.price=price;
        Item.all.add(this);


    }
    public int getId() {
        return this.id;
    } 
    public String getTitle() {
        return this.name;
    }
    public int getMenuId() {
        return this.menuId;
    }
    
}
